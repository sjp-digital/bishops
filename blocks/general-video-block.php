<div class="general-video-block">
	<div class="video-image video-module<? if(get_sub_field('additional_videos', $post->ID)): ?> no-thumbnails<?endif; ?>" data-fancybox>
		<a href="<? the_sub_field('video_url', $post->ID); ?>" style="background: url('<? the_sub_field('main_video_image', $post->ID); ?>') center / cover;" data-fancybox></a>
	</div>


	<? if(get_sub_field('additional_videos', $post->ID)): ?>
		<div class="video-thumbnails">
			<? while ( have_rows('video_thumbnails', $post->ID) ) : the_row(); ?>
				<div class="thumbnail video-module">
					<a href="<? the_sub_field('thumbnail_video_url', $post->ID); ?>" style="background: url('<? the_sub_field('thumbnail_video_image', $post->ID); ?>') center / cover;" data-fancybox></a>
				</div>
			<? endwhile; ?>
		</div>
	<? endif; ?>
</div>

