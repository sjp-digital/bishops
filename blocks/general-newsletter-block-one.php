<div class="newsletter-block newsletter-block-one" style="background-image: url('<? the_field('background_image', 254); ?>');">
	<div class="wrap">
		<div class="content">
			<? the_field('newsletter_content', 254); ?>

			<? echo do_shortcode('[contact-form-7 id="260" title="Newsletter Signup"]') ?>
		</div>
	</div>
</div>
