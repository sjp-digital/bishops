<div class="feature-content-accordion">
	<div class="wrap">
		<? if(get_sub_field('accordion_title', $post->ID)): ?>
			<h2><? the_sub_field('accordion_title', $post->ID); ?></h2>
		<? endif; ?>

		<div class="accordion">
			<? while ( have_rows('content_accordion', $post->ID) ) : the_row(); ?>
				<? $fulltitle = get_sub_field('content_title', $post->ID); ?>
				<? $fulltitle = str_replace(" ", "-", $fulltitle); ?>
				<? $fulltitle = preg_replace('/[^A-Za-z0-9\-]/', '', $fulltitle); ?>
				<? $fulltitle = strtolower($fulltitle); ?>

				<div class="accordion-row" id="<?= $fulltitle; ?>">
					<h3 class="accordion-title"><? the_sub_field('content_title', $post->ID); ?></h3>

					<div class="inner-accordion">
						<div class="flex-container">
							<div class="content">
								<? if(get_sub_field('content_strapline', $post->ID)): ?>
									<h4><? the_sub_field('content_strapline', $post->ID); ?></h4>
								<? endif; ?>

								<? the_sub_field('accordion_content', $post->ID); ?>

								<? $btn = get_sub_field('button', $post->ID); ?>
								<? if($btn): ?>
									<a href="<?php echo $btn['url']; ?>" target="<?= $btn['target']; ?>" class="btn small gold <? the_sub_field('select_button_position', $post->ID) ?>"><span><?php echo $btn['title']; ?></span></a>
								<? endif; ?>
							</div>
						</div>
					</div>
				</div>
			<? endwhile; ?>
		</div>
	</div>
</div>




