<div class="feature-contact-form">
	<div class="wrap">
		<? if(get_sub_field('feature_contact_form_title', $post->ID)): ?>
			<h3><? the_sub_field('feature_contact_form_title', $post->ID); ?></h3>
		<? endif; ?>

		<div class="columns">
			<div class="content">
				<? the_sub_field('feature_contact_form_content', $post->ID); ?>
			</div>

			<? $formshortcode = get_sub_field('contact_form_shortcode', $post->ID); ?>
			<?= do_shortcode($formshortcode); ?>
		</div>
	</div>
</div>

