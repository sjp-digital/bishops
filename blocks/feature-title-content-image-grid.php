<div class="feature-title-content-image-grid">
	<div class="wrap">
		<h2><? the_sub_field('feature_title_content_image_grid_title', $post->ID); ?></h2>

		<div class="content <? the_sub_field('number_of_columns', $post->ID); ?>">
			<? the_sub_field('feature_title_content_image_grid_content', $post->ID); ?>
		</div>

		<? $images = get_sub_field('feature_title_content_image_images', $post->ID); ?>

		<? if( $images ): ?>
			<div class="gallery">
				<? $i = 0; ?>
				<?php foreach( $images as $image ): ?>
					<? if($i == 1):?>
						<div class="central-col">
					<? endif; ?>
				    <div class="image">
				    	<a href="<?= $image; ?>" data-fancybox="images" class="inner-image" style="background: url('<?= $image; ?>') center / cover;"></a>
				    </div>

					<? if($i == 3):?>
						</div>
					<? endif; ?>
				<? $i++; ?>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</div>



