<div class="admissions" style="background: url('<? the_field('background_image', 169); ?>') top center / cover;">
	<div class="wrap">
		<h3><hr><? the_field('title', 169); ?></h3>

		<? $btn = get_field('button', 169); ?>

		<? if($btn): ?>
			<a href="<?php echo $btn['url']; ?>" target="<?= $btn['target']; ?>" class="btn gold"><span><?php echo $btn['title']; ?></span></a>
		<? endif; ?>
	</div>
</div>
