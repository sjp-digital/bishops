<div class="content-block<? if(get_sub_field('content_border_top', $post->ID)): ?> border-top<? endif; ?>">
	<div class="wrap">
		<? if(get_sub_field('section_navigation', $post->ID)): ?>
			<div class="in-page-nav">

				<? if ( is_page() && $post->post_parent ): ?>
	    			<? $identifierID = wp_get_post_parent_id($post->ID) ?>
				<? else: ?>
					<? $identifierID = $post->ID; ?>
				<? endif; ?>

				<ul>
					<li class="parent-page"><a href="<?= get_permalink($identifierID) ?>"><?= get_the_title($identifierID) ?></a>
						<ul class="sub-menu">
							<? wp_list_pages( array(
								'sort_column' => 'menu_order',
								'child_of' => $identifierID,
								'depth' => -1,
								'title_li' => false
							) ); ?>
						</ul>
					</li>
				</ul>

				<h3 class="mobile-page-title"><?= get_the_title($post->ID); ?></h3>
			</div>
		<? endif; ?>

		<div class="content">
			<? while ( have_rows('content_blocks', $post->ID) ) : the_row();

				if( get_row_layout() == 'main_title' ):

					include block('content-main-title.php');

				elseif( get_row_layout() == 'sub_title' ):

					include block('content-sub-title.php');

				elseif( get_row_layout() == 'content' ):

					include block('content-content.php');

				elseif( get_row_layout() == 'full_width_image' ):

					include block('content-full-width-image.php');

				elseif( get_row_layout() == 'author' ):

					include block('content-author.php');

				elseif( get_row_layout() == 'video_block' ):

					include block('general-video-block.php');

				elseif( get_row_layout() == 'button' ):

					include block('content-button.php');

				elseif( get_row_layout() == 'button_columns' ):

					include block('content-button-columns.php');

				elseif( get_row_layout() == 'content_contact_details' ):

					include block('content-contact-details.php');

				endif; ?>

			<? endwhile; ?>
		</div>
	</div>
</div>
