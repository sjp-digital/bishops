<div class="feature-equipment">
	<div class="wrap">
		<h3><? the_sub_field('feature_equipment_title', $post->ID); ?></h3>
		<p><? the_sub_field('feature_equipment_introductory_text', $post->ID); ?></p>

		<div class="image" style="background-image: url('<? the_sub_field('feature_equipment_image', $post->ID); ?>');">
			<? $btn = get_sub_field('feature_equipment_button', $post->ID); ?>

			<? if($btn): ?>
				<a href="<?php echo $btn['url']; ?>" target="<?= $btn['target']; ?>" class="btn small gold"><span><?php echo $btn['title']; ?></span></a>
			<? endif; ?>
		</div>
	</div>
</div>
