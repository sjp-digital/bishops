<div class="feature-flexible-three-columns feature-columns">
	<div class="wrap">
		<? if(get_sub_field('feature_flexible_columns_title', $post->ID)): ?>
			<h2><? the_sub_field('feature_flexible_columns_title', $post->ID); ?></h2>
		<? endif; ?>

		<div class="columns">
			<? while ( have_rows('feature_flexible_columns_columns') ) : the_row(); ?>
				<div class="column">
					<h3><? the_sub_field('column_title', $post->ID); ?><? if(get_sub_field('column_sub_title', $post->ID)): ?><span><? the_sub_field('column_sub_title', $post->ID); ?></span><? endif; ?></h3>
					<? the_sub_field('column_content', $post->ID); ?>
				</div>
			<? endwhile; ?>
		</div>

		<? if(get_sub_field('image', $post->ID)): ?>
			<img src="<? the_sub_field('image', $post->ID) ?>" alt="<?= get_the_title($post->ID)?> Image">
		<? endif; ?>

		<? if(get_sub_field('feature_flexible_columns_lower_columns', $post->ID)): ?>
			<div class="columns">
				<? while ( have_rows('feature_flexible_columns_lower_columns') ) : the_row(); ?>
					<div class="column">
						<h3><? the_sub_field('column_title', $post->ID); ?><? if(get_sub_field('column_sub_title', $post->ID)): ?><span><? the_sub_field('column_sub_title', $post->ID); ?></span><? endif; ?></h3>
						<? the_sub_field('column_content', $post->ID); ?>
					</div>
				<? endwhile; ?>
			</div>
		<? endif; ?>
	</div>
</div>
