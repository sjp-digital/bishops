<div class="feature-image-and-content">
	<div class="wrap">
		<? if(get_sub_field('feature_image_and_content_block_title', $post->ID)): ?>
			<h2><? the_sub_field('feature_image_and_content_block_title', $post->ID); ?></h2>
		<? endif; ?>

		<? if(get_sub_field('feature_image_and_content_introductory_content', $post->ID)): ?>
			<h4><? the_sub_field('feature_image_and_content_introductory_content', $post->ID); ?></h4>
		<? endif; ?>

		<? while ( have_rows('content_rows') ) : the_row(); ?>
			<div class="content-row">
				<img src="<? the_sub_field('feature_image_and_content_image', $post->ID); ?>" alt="<? the_sub_field('feature_image_and_content_title', $post->ID); ?> Image">

				<div class="content">
					<h3><? the_sub_field('feature_image_and_content_title', $post->ID); ?></h3>
					<img src="<? the_sub_field('feature_image_and_content_image', $post->ID); ?>" alt="<? the_sub_field('feature_image_and_content_title', $post->ID); ?> Image">

					<? the_sub_field('feature_image_and_content_content', $post->ID); ?>

					<? $btn = get_sub_field('feature_image_and_content_button', $post->ID); ?>

					<? if($btn): ?>
						<a href="<?php echo $btn['url']; ?>" target="<?= $btn['target']; ?>" class="btn small gold"><span><?php echo $btn['title']; ?></span></a>
					<? endif; ?>
				</div>
			</div>
		<? endwhile; ?>
	</div>
</div>


