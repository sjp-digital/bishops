<div class="standalone-ofsted-view content-block border-top">
	<div class="wrap">
		<div class="content">
			<h1>Ofsted View</h1>

			<?php if ( ! post_password_required() ): ?>
				<div class="left-content">
					<? the_sub_field('logged-in_content', $post->ID); ?>
				</div>
			<? else: ?>
			<div class="login">
				<p>Please sign in to access the Ofsted specific content.</p>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>

			<? endif; ?>
		</div>
	</div>

	<?php if ( ! post_password_required() ): ?>
		<div class="link-grid">
			<div class="wrap">
				<? while ( have_rows('links') ) : the_row(); ?>
					<div class="link">
						<h3 class="mobile-title"><? the_sub_field('link_title'); ?></h3>
						<div class="image" style="background: url('<? the_sub_field('link_image'); ?>') center / cover;"></div>

						<div class="link-content">
							<h3><? the_sub_field('link_title'); ?></h3>

							<a class="btn gold" target="_blank" href="<? the_sub_field('link'); ?>">Visit ></a>
						</div>
					</div>

				<? endwhile; ?>
			</div>
		</div>
	<? endif; ?>
</div>
