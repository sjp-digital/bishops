<div class="navigation-modules">
	<? while ( have_rows('navgiation_modules') ) : the_row(); ?>
		<? $link = get_sub_field('navgiation_modules_link', $post->ID); ?>

		<div class="module">
			<div class="inner-image" style="background: url('<? the_sub_field('navgiation_modules_background_image', $post->ID); ?>') center / cover;"></div>
			<a href="<?php echo $link['url']; ?>" target="<?= $link['target']; ?>">
				<h4><? the_sub_field('navgiation_modules_title', $post->ID); ?></h4>
			</a>
		</div>
	<? endwhile; ?>
</div>

