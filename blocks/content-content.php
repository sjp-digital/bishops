	<div class="main-content-container">
		<div class="main-content <? the_sub_field('number_of_columns', $post->ID); ?>-col">
			<? the_sub_field('content', $post->ID); ?>
		</div>

		<? $btn = get_sub_field('button', $post->ID); ?>
		<? if($btn): ?>
			<a href="<?php echo $btn['url']; ?>" target="<?= $btn['target']; ?>" class="btn small gold"><span><?php echo $btn['title']; ?></span></a>
		<? endif; ?>
	</div>

