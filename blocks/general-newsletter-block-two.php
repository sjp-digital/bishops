<div class="newsletter-block newsletter-block-two" style="background: linear-gradient(to bottom, rgba(65, 97, 148, 0.7), rgba(65, 97, 148, 0.7)), url('<? the_field('background_image', 253); ?>') center / cover;">
	<div class="wrap">
		<div class="content">
			<? the_field('newsletter_content', 253); ?>

			<? echo do_shortcode('[contact-form-7 id="260" title="Newsletter Signup"]') ?>
		</div>
	</div>
</div>
