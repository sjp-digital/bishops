<div class="button-columns">
	<? while ( have_rows('buttons') ) : the_row(); ?>
		<? $btn = get_sub_field('button', $post->ID); ?>
		<a href="<?php echo $btn['url']; ?>" target="<?= $btn['target']; ?>" class="content-btn btn small gold <? the_sub_field('select_button_position', $post->ID) ?>"><span><?php echo $btn['title']; ?></span></a>
	<? endwhile; ?>
</div>


