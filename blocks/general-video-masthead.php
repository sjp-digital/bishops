<div class="video-masthead" style="background: url('<? the_sub_field('video_masthead_background_image', $post->ID); ?>') center / cover;">
	<? if(get_sub_field('video_masthead_video', $post->ID)): ?>
		<a href="<? the_sub_field('video_masthead_video_url', $post->ID); ?>" data-fancybox>
	<? endif; ?>

	<div class="wrap">
		<? the_sub_field('video_masthead_masthead_content', $post->ID); ?>
	</div>

	<? if(get_sub_field('video_masthead_video', $post->ID)): ?>
		</a>
	<? endif; ?>
</div>






