<div class="content-contact-details">
	<? the_field('iframe_code', 'option') ?>

	<div class="contact-details">
		<address>
			<? the_field('school_name', 'option') ?><br>
			<? the_field('address', 'option') ?>
		</address>

		<p>Tel: <a href="tel:<? the_field('telephone_number', 'option') ?>"><? the_field('telephone_number', 'option') ?></a><br>
		Email: <a href="mailto:<? the_field('email_address', 'option') ?>"><? the_field('email_address', 'option') ?></a>
	</div>
</div>



