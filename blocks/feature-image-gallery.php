<div class="feature-image-gallery">
	<div class="wrap">
		<? if(get_sub_field('image_gallery_title', $post->ID)): ?>
			<h3><? the_sub_field('image_gallery_title', $post->ID); ?></h3>
		<? endif; ?>

		<? $images = get_sub_field('image_gallery'); ?>
		<div class="main-images">
	 		<?php foreach( $images as $image ): ?>
		 			<div class="image">
		 				<?= wp_get_attachment_image( $image, 'full' ); ?>
		 			</div>
			<? endforeach; ?>
		</div>

		<div class="image-gallery-carousel">
	 		<?php foreach( $images as $image ): ?>
		 			<div class="item">
		 				<?= wp_get_attachment_image( $image, 'full' ); ?>
		 			</div>
			<? endforeach; ?>
		</div>
	</div>
</div>
