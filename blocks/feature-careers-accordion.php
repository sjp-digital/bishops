<div class="feature-careers-accordion">
	<div class="wrap">
		<div class="accordion">
			<? while ( have_rows('careers', 1007) ) : the_row(); ?>
				<div class="accordion-row">
					<h3 class="accordion-title"><? the_sub_field('career_title', 1007); ?></h3>

					<div class="inner-accordion">
						<div class="flex-container">
							<div class="content">
								<h4><? the_sub_field('career_strapline', 1007); ?></h4>
								<? the_sub_field('career_description', 1007); ?>

								<div class="btn-container">
									<? $headmasters_btn = get_sub_field('headmasters_letter', 1007); ?>
									<? if($headmasters_btn): ?>
										<a href="<?php echo $headmasters_btn; ?>" class="btn small gold" download>Headteacher's Letter</a>
									<? endif; ?>

									<? $job_desc = get_sub_field('job_description', 1007); ?>
									<? if($job_desc): ?>
										<a href="<?php echo $job_desc; ?>" class="btn small gold" download>Job Description</a>
									<? endif; ?>

									<? $person_spec = get_sub_field('person_specification', 1007); ?>
									<? if($person_spec): ?>
										<a href="<?php echo $person_spec; ?>" class="btn small gold" download>Person Specification</a>
									<? endif; ?>
								</div>
							</div>

							<div class="form-col">
								<h4>Apply for this role</h4>
								<?= do_shortcode('[contact-form-7 id="1019" title="Careers Form"]') ?>
							</div>
						</div>
					</div>
				</div>
			<? endwhile; ?>
		</div>
	</div>
</div>




