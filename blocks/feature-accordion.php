<div class="feature-accordion">
	<div class="wrap">
		<div class="accordion">
			<? while ( have_rows('accordion_row') ) : the_row(); ?>
				<div class="accordion-row" id="<? the_sub_field('row_anchor', $post->ID) ?>">
					<h3 class="accordion-title"><? the_sub_field('row_title'); ?></h3>

					<div class="inner-accordion">
						<? while ( have_rows('table') ) : the_row(); ?>
							<div class="inner-accordion-row">
								<div class="column">
									<p><? the_sub_field('column_one'); ?></p>
								</div>
								<div class="column">
									<p><? the_sub_field('column_two'); ?></p>
								</div>
								<div class="column">
									<p><? the_sub_field('column_three'); ?></p>
								</div>
							</div>

						<? endwhile; ?>
					</div>
				</div>
			<? endwhile; ?>
		</div>
	</div>
</div>




