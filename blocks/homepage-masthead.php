<div class="block homepage-masthead" style="background: url('<? the_sub_field('homepage_masthead_background_image', $post->ID) ?>') center / cover;">
	<div class="wrap">
		<? the_sub_field('homepage_masthead_content', $post->ID) ?>
		<hr>

		<div class="modules">
			<? while ( have_rows('homepage_masthead_key_modules') ) : the_row(); ?>
				<div class="module">
					<div class="inner-image" style="background: url('<? the_sub_field('homepage_masthead_module_background_image'); ?>') center / cover;"></div>
					<a href="<? the_sub_field('homepage_masthead_module_link'); ?>">
						<div class="content">
							<h3><? the_sub_field('homepage_masthead_module_title'); ?></h3>
							<hr>
						</div>
					</a>
				</div>
			<? endwhile; ?>
		</div>
	</div>
</div>

