<div class="social-strip" style="background: linear-gradient(to bottom, rgba(65, 97, 148, 0.7), rgba(65, 97, 148, 0.7)), url('<? the_field('background_image', 331); ?>') center / cover;">
	<div class="wrap">
		<div class="content">
			<? if(get_sub_field('newsletter_content', $post->ID)): ?>
				<? the_sub_field('newsletter_content', $post->ID); ?>
			<? else: ?>
				<? the_field('newsletter_content', 331); ?>
			<? endif; ?>
		</div>
	</div>
</div>
