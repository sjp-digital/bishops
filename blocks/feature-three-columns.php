<div class="feature-three-columns feature-columns">
	<div class="wrap">
		<h2><? the_sub_field('feature_-_three_columns_title', $post->ID); ?></h2>

		<div class="columns">
			<? while ( have_rows('feature_-_three_columns_columns') ) : the_row(); ?>
				<div class="column">
					<h3><? the_sub_field('column_title', $post->ID); ?></h3>
					<? the_sub_field('column_content', $post->ID); ?>
				</div>
			<? endwhile; ?>
		</div>
	</div>
</div>
