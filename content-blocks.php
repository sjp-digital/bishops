	<? while ( have_rows('blocks', $post->ID) ) : the_row();

		if( get_row_layout() == 'homepage_masthead' ):

			include block('homepage-masthead.php');

		elseif( get_row_layout() == 'homepage_introduction' ):

			include block('homepage-introduction.php');

		elseif( get_row_layout() == 'news_feed' ):

			include block('homepage-news-feed.php');

		elseif( get_row_layout() == 'admissions' ):

			include block('general-admissions.php');

		elseif( get_row_layout() == 'calendar' ):

			include block('general-calendar.php');

		elseif( get_row_layout() == 'internal_masthead' ):

			include block('general-internal-masthead.php');

		elseif( get_row_layout() == 'content' ):

			include block('content.php');

		elseif( get_row_layout() == 'navigation_modules' ):

			include block('general-navigation-modules.php');

		elseif( get_row_layout() == 'feature_-_three_columns' ):

			include block('feature-three-columns.php');

		elseif( get_row_layout() == 'feature_flexible_columns' ):

			include block('feature-flexible-columns.php');

		elseif( get_row_layout() == 'newsletter_block_one' ):

			include block('general-newsletter-block-one.php');

		elseif( get_row_layout() == 'newsletter_block_two' ):

			include block('general-newsletter-block-two.php');

		elseif( get_row_layout() == 'feature_-_contact_form' ):

			include block('feature-contact-form.php');

		elseif( get_row_layout() == 'feature_-_pull_out_quote' ):

			include block('feature-pull-out-quote.php');

		elseif( get_row_layout() == 'feature_image_and_content' ):

			include block('feature-image-and-content.php');

		elseif( get_row_layout() == 'feature_equipment' ):

			include block('feature-equipment.php');

		elseif( get_row_layout() == 'feature-accordion' ):

			include block('feature-accordion.php');

		elseif( get_row_layout() == 'feature_careers_accordion' ):

			include block('feature-careers-accordion.php');

		elseif( get_row_layout() == 'feature_content_accordion' ):

			include block('feature-content-accordion.php');

		elseif( get_row_layout() == 'feature_image_gallery' ):

			include block('feature-image-gallery.php');

		elseif( get_row_layout() == 'social_strip' ):

			include block('general-social-strip.php');

		elseif( get_row_layout() == 'video_masthead' ):

			include block('general-video-masthead.php');

		elseif( get_row_layout() == 'video_block' ):

			include block('general-video-block.php');

		elseif( get_row_layout() == 'button' ):

			include block('content-button.php');

		elseif( get_row_layout() == 'feature_title_content_image_grid' ):

			include block('feature-title-content-image-grid.php');

		elseif( get_row_layout() == 'standalone_ofsted_view' ):

			include block('standalone-ofsted-view.php');

		elseif( get_row_layout() == 'general_-_twitter_feed' ):

			include block('general-twitter-feed.php');

		endif;

	endwhile; ?>
