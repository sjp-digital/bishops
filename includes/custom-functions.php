<?php

# =========== STYLE LOGIN =========== #
function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/dist/login-style.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

add_filter( 'login_headerurl', 'codecanal_loginlogo_url' );
function codecanal_loginlogo_url($url)
{
  return home_url();
}

# =========== LATEST NEWS =========== #
	function latest_news() {
		$args = array(
			'post_status'    	=> 'publish',
			'post_type'        	=> 'post',
			'posts_per_page'	=> '6',
		);

		$query = new WP_Query($args);

		foreach($query->posts as $post): ?>
			<?php setup_postdata( $post ); ?>

			<?php echo get_the_title($post->ID) ?>
			<?php echo wp_trim_words($post->post_content, 10)?>
			<?php echo get_the_permalink($post->ID) ?>

		<?php endforeach;

		wp_reset_query();
	}

# =========== GET NEXT EVENT =========== #
	function get_next_event() {
		$args = array(
			'post_status'		=> 'publish',
			'post_type'			=> 'events',
			'posts_per_page' 	=> '1',
			'orderby'   		=> 'meta_value',
	        'meta_key' 			=> 'date_of_event',
	        'order' 			=> 'ASC',
	        'meta_query' 		=> array(
				array(
					'key' 		=> 'date_of_event',
					'value' 	=> date('Ymd'),
					'type' 		=> 'DATE',
					'compare' 	=> '>='
				),
			),
		);

		$query = new WP_Query($args);
		foreach($query->posts as $post):
			$date_of_event =  get_field('date_of_event', $post->ID);
			$start_time = get_field('start_time', $post->ID);
			$end_time = get_field('end_time', $post->ID);

			$date_of_event = new DateTime($date_of_event);
			$start_time = new DateTime($start_time);
			$end_time = new DateTime($end_time); ?>


		<div class="upcoming-event">
			<div class="event-head">
				<div class="left-col">
					<h4>Upcoming next</h4>
					<p><?php echo $date_of_event->format('j l, F'); ?></p>
				</div>
				<h3><?= get_the_title($post->ID) ?></h3>
			</div>
			<div class="event-content">
				<? the_field('description', $post->ID); ?>
			</div>
		</div>
		<?php endforeach;
	}

# =========== GET EVENTS =========== #
	function get_events() {
		$args = array(
			'post_status'    => 'publish',
			'post_type'        => 'events',
			'nopaging'	=> true,
			'posts_per_page' => '-1',
			'orderby'   => 'date_of_event',
	        'meta_key' => 'date_of_event',
	        'order' => 'ASC',
	        'meta_query' => array(
				array(
					'key' => 'date_of_event',
					'value' => date('Ymd'),
					'type' => 'DATE',
					'compare' => '>='
				),
			),
		);

		$query = new WP_Query($args);
		$i=0;
		foreach($query->posts as $post):
			$date_of_event =  get_field('date_of_event', $post->ID);
			$start_time = get_field('start_time', $post->ID);
			$end_time = get_field('end_time', $post->ID);

			$date_of_event = new DateTime($date_of_event); ?>

			<?php if($i == 0): $month = date("M", strtotime(get_field("date_of_event", $post->ID))); ?>
				<div class="month-block">
					<div class="title-container">
						<img class="slick-prev-arrow" src="<? image('slick-prev-arrow.svg') ?>"><h3><?= $date_of_event->format('F Y'); ?></h3><img class="slick-next-arrow" src="<? image('slick-next-arrow.svg') ?>">
					</div>
			<?php endif; ?>

			<?php if($month != date("M", strtotime(get_field("date_of_event", $post->ID)))):
				$month = date("M", strtotime(get_field("date_of_event", $post->ID)))?>
				</div>
				<div class="month-block">
					<div class="title-container">
						<img class="slick-prev-arrow" src="<? image('slick-prev-arrow.svg') ?>"><h3><?= $date_of_event->format('F Y'); ?></h3><img class="slick-next-arrow" src="<? image('slick-next-arrow.svg') ?>">
					</div>
			<?php endif; ?>

			<?php if($i == 0): $day = date("d", strtotime(get_field("date_of_event", $post->ID))); ?>
				<div class="day-block">
					<h4><?= $date_of_event->format('j l, F'); ?></h4>
			<?php endif; ?>

			<?php if($day != date("d", strtotime(get_field("date_of_event", $post->ID)))):
				$day = date("d", strtotime(get_field("date_of_event", $post->ID)))?>
				<div class="day-block">
					<h4><?= $date_of_event->format('j l, F'); ?></h4>
			<?php endif; ?>

				<div class="event">
					<div class="event-title">
						<p><?= get_the_title($post->ID) ?></p>
						<p><? the_field('location', $post->ID); ?></p>
					</div>
					<div class="location">
						<p><? the_field('location', $post->ID); ?></p>
					</div>
					<div class="timing">
						<p><?= $start_time; ?> - <?= $end_time; ?></p>
					</div>
				</div>

			<?php if($i == 0): $day = date("d", strtotime(get_field("date_of_event", $post->ID))); ?>
				</div>
			<? endif; ?>
		<?php $i++; ?>
		<?php endforeach; ?>
	<? }

# =========== OFSTED PAGE LOGIN =========== #
function my_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post" class="password-form">
    <input name="post_password" placeholder="Password" id="' . $label . '" type="password" size="20" maxlength="20" /><button class="btn gold">' . esc_attr__( "Sign in >" ) . '</button></form>';
    return $o;
}
add_filter( 'the_password_form', 'my_password_form' );


function custom_password_cookie_expiry( $expires ) {
    return 0;  // Make it a session cookie
}
add_filter( 'post_password_expires', 'custom_password_cookie_expiry' );

# =========== EXCLUDE FROM POSTS =========== #
add_action('pre_get_posts','wpse67626_exclude_posts_from_search');
function wpse67626_exclude_posts_from_search( $query ){

    if( $query->is_main_query() && is_search() ){
         //Exclude posts by ID
         $post_ids = array(440);
         $query->set('post__not_in', $post_ids);
    }
}
add_action( 'init', 'searchexclusion', 99 );

function searchexclusion() {
	global $wp_post_types;
    $wp_post_types['templates']->exclude_from_search = true;
    $wp_post_types['events']->exclude_from_search = true;
}

# =========== ADD ACF OPTIONS =========== #
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

# =========== REMOVE AUTI P CF7 =========== #
    add_filter('wpcf7_autop_or_not', '__return_false');

# =========== MOVE YOAST TO BOTTOM =========== #
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');
