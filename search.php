<?php get_header(); ?>
	<?
	global $wp_query;
	if(isset($_GET['s'])) {
		$search_results = $_GET['s'];
	} ?>

	<div class="content-block">
		<div class="wrap">
			<div class="content">
				<h1>Search results for: <?= $search_results; ?></h1>

				<p><? echo $wp_query->found_posts.' results found.'; ?></p>

				<div class="results-container">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="result">
							<h4 class="search-title entry-title"><?php the_title(); ?></h4>

							<? if(get_field('search_result_content', $post->ID)): ?>
								<p><? the_field('search_result_content', $post->ID) ?></p>
							<? endif; ?>

							<p><a href="<?php the_permalink() ?>">Read more ></a></p>
						</div>
					<?php endwhile; ?>

					<?php bones_page_navi(); ?>

					<? else: ?>
						<p><?php _e( 'Sorry, No Results.', 'bonestheme' ); ?></p>
						<p><?php _e( 'Try your search again.', 'bonestheme' ); ?></p>
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
