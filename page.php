<?php get_header(); ?>
	<?php if(!post_password_required()) { ?>
		<? include 'content-blocks.php'; ?>
	<?php } else { ?>
		<div class="content-block">
			<div class="wrap">
				<div class="content">
					<?php the_content() ?>
				</div>
			</div>
		</div>
	<?php } ?>
<? get_footer(); ?>
