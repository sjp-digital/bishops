<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php wp_title(''); ?></title>
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>

        <script type="text/javascript">
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-106579143-1', 'auto');
            ga('send', 'pageview', {
                dimension1: '8964623',
                dimension2: 'public',
                dimension3: '160108',
                dimension4: '00000000-0000-0000-0000-000000004159',
                dimension5: 'www.bishopschester.co.uk'
            });
        </script>

		<link rel="stylesheet" href="https://use.typekit.net/bzi3obf.css">
        <script src="https://kit.fontawesome.com/d8ddbdf770.js"></script>

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
		<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
		<script>
		window.addEventListener("load", function(){
		window.cookieconsent.initialise({
		  "palette": {
		    "popup": {
		      "background": "#041A3E",
		      "text": "#ffffff"
		    },
		    "button": {
		      "background": "#5CC78E",
		      "text": "#000"
		    }
		  },
		  "theme": "classic",
		  "position": "bottom-right",
		  "content": {
		    "href": "/cookies-policy/"
		  }
		})});
		</script>
    </head>

	<body <?php body_class(); ?>>
		<script>
    		var templateDir = '<?php echo get_template_directory_uri() ?>/dist/';
    	</script>

		<header>
			<div class="wrap">
				<div class="top-row">
					<div class="mobile-quick-links-close">
						<img src="<? image('quick-links-close.svg') ?>" alt="Quick Links Close">
					</div>

					<?php wp_nav_menu(array(
						'menu' => __( 'Secondary Menu', 'bonestheme' ),
						'theme_location' => 'secondary-nav',
					)); ?>
					<?php get_search_form(); ?>


					<ul class="additional-links">
						<? while ( have_rows('quick_links', 'options') ) : the_row(); ?>
							<? $nav_link = get_sub_field('link', 'options'); ?>
							<li><a href="<?php echo $nav_link['url']; ?>" target="<?= $nav_link['target']; ?>"><img src="<? the_sub_field('link_icon', 'options'); ?>" alt="<? the_sub_field('link_title', 'options'); ?> Link"><? the_sub_field('link_title', 'options'); ?></a></li>
						<? endwhile; ?>
					</ul>
				</div>
				<div class="lower-row">
					<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow"><img src="<? image('logo.svg') ?>" alt="logo"></a>

					<nav>
						<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow"><img src="<? image('logo.svg') ?>" alt="logo"></a>

						<?php wp_nav_menu(array(
							'menu' => __( 'The Main Menu', 'bonestheme' ),
							'theme_location' => 'main-nav',
						)); ?>

						<?php get_search_form(); ?>
					</nav>

					<div class="menu-toggle">
						<div class="hamburger">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<div class="cross">
							<span></span>
							<span></span>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div class="mobile-quick-links-trigger">
			<img src="<? image('quick-link-trigger.svg') ?>" alt="Quick Links Trigger">
		</div>


		<? if(get_field('covid_link', 'options') && is_front_page()): ?>
			<? $btn = get_field('covid_link', 'options'); ?>

			<a class="btn covid-link secondary" href="<?php echo $btn['url']; ?>"><?php echo $btn['title']; ?></a>
		<? endif; ?>
