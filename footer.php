			<footer>
				<div class="wrap footer-logo">
					<img src="<? image('footer-logo.svg') ?>" alt=" Footer Logo">
				</div>
				<div class="wrap columns">
					<? the_field('iframe_code', 'option') ?>

					<div class="right-col">
						<div class="top-row">
							<div class="contact-details">
								<address>
								<? the_field('address', 'option') ?>
								</address>
								<p>Tel: <a href="tel:<? the_field('telephone_number', 'option') ?>"><? the_field('telephone_number', 'option') ?></a><br>
								Email: <a href="mailto:<? the_field('email_address', 'option') ?>"><? the_field('email_address', 'option') ?></a>

							</div>

							<?php wp_nav_menu(array(
								'menu' => __( 'Footer Links', 'bonestheme' ),
								'theme_location' => 'footer-links',
							)); ?>

							<div class="social-container">
								<a target="_blank" href="<? the_field('facebook', 'option') ?>"><img src="<? image('facebook.svg') ?>" alt="Facebook"></a>
								<a target="_blank" href="<? the_field('twitter', 'option') ?>"><img src="<? image('twitter.svg') ?>" alt="Twitter"></a>
								<a target="_blank" href="<? the_field('instagram', 'option') ?>"><img src="<? image('instagram.svg') ?>" alt="Instagram"></a>
								<a target="_blank" href="<? the_field('ceop', 'option') ?>"><img src="<? image('ceop.svg') ?>" alt="CEOP"></a>
							</div>
						</div>

						<img class="accreditiations" src="<? the_field('accreditations', 'option'); ?>" alt="Accreditations">
					</div>
				</div>
			</footer>

		<?php wp_footer(); ?>

	</body>

</html>
