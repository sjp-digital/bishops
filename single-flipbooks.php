<?php get_header(); ?>

	<div class="flipbook-container">
		<div class="wrap">
			<div id="PDF_Container" class="flipbook-container" data-pdf="<? the_field('upload_flipbook_pdf',$post->ID); ?>"></div>
		</div>
	</div>

<?php get_footer(); ?>
