jQuery(document).ready(function($) {
	var windowWidth = $(window).width()

/* ====================== QUICK LINKS ======================  */
	$('.mobile-quick-links-trigger img').on("click", function() {
		$('.top-row').toggleClass('active');
		$('body').toggleClass('noscroll');
	})

	$('.mobile-quick-links-close img').on("click", function() {
		$('body').toggleClass('noscroll');
		$('.top-row').toggleClass('active');
	})

/* ====================== MOBILE NAV ======================  */
	$('.menu-toggle').on("click", function() {
		$(this).toggleClass('active');
		$('body').toggleClass('noscroll');
		$('header nav').toggleClass('active');
	})

	if(windowWidth < 1024) {
		$('#menu-main-menu .menu-item-has-children').on("click", function() {
		   $(this).toggleClass('expanded');
		   $(this).find('.sub-menu').slideToggle();
		})
	}

/* ====================== SUB MENU NAV ======================  */
	if(windowWidth < 1024) {
		$('.sub-menu-chevron').on("click", function() {
			$(this).toggleClass('active');
			$('.sub-menu').slideToggle();
		})

		$('.in-page-nav .parent-page').on("click", function() {
			$(this).toggleClass('active');
			$('.sub-menu').slideToggle();
		})
	}

/* ====================== ACCORDIONS ======================  */
	$('.accordion .accordion-title').on("click", function() {
		if($(this).hasClass('active')) {
			$(this).removeClass('active');
			$(this).next('.inner-accordion').slideUp(500);
		} else {
			$('.inner-accordion').slideUp(500);
			$('.accordion .accordion-title').removeClass('active');
			$(this).next('.inner-accordion').slideDown(500);
			$(this).addClass('active');
		}
	})

	function accordionAnchoring() {
	    var parts = window.location.href.split('#');
	    if (parts.length > 1) {
	        parts = parts[1];

	        if (parts.length > 1) {
	            var selected = $('#'+parts+'');
	            $(selected).find('.inner-accordion').addClass('active');
	            $(selected).find('h3').addClass('active');
	            $(selected).find('.inner-accordion').slideDown();
	            $(selected).addClass('active');
	        }
	    }
	}

	accordionAnchoring()

/* ====================== CALENDAR CAROUSEL ======================  */
	$('.calendar-carousel').slick({
		slidesToShow: 1,
		dots: false,
		arrows: false,
		fade: true,
		cssEase: 'linear'
	});
	$('.slick-prev-arrow').on("click", function() {
	   $('.calendar-carousel').slick('slickPrev');
	})

	$('.slick-next-arrow').on("click", function() {
	   $('.calendar-carousel').slick('slickNext');
	})

/* ====================== CAREER POPULATE FORM ======================  */
	$( ".accordion-row").each(function() {
		var careerTitle = $(this).find('h3').text();
		$(this).find('input[name="career-applied-for"]').val(careerTitle);
	});

/* ====================== CV/ LETTER CHANGE ======================  */
  $("#your-cv, #your-support-letter").on("change", function() {
	var filename = this.value
	filename = filename.split('\\');
	filename.reverse();

	console.log(filename[0]);

	$(this).parents('.upload-file').find('.filename').html(filename[0]);
	$(this).parents('.upload-file').find('.filename-container').addClass('active');
  })

/* ====================== SMOOTH SCROLL =======================  */
	$('a[href*="#"]')
	  // Remove links that don't actually link to anything
	  .not('[href="#"]')
	  .not('[href="#0"]')
	  .click(function(event) {
		// On-page links
		if (
		  location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
		  &&
		  location.hostname == this.hostname
		) {
		  // Figure out element to scroll to
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		  // Does a scroll target exist?
		  if (target.length) {
			// Only prevent default if animation is actually gonna happen
			event.preventDefault();
			$('html, body').animate({
			  scrollTop: target.offset().top
			}, 1000, function() {
			  // Callback after animation
			  // Must change focus!
			  var $target = $(target);
			  $target.focus();
			  if ($target.is(":focus")) { // Checking if the target was focused
				return false;
			  } else {
				$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
				$target.focus(); // Set focus again
			  };
			});
		  }
		}
	  });

  //========== TEXTAREA EXPAND ON TYPE ==========//
  autosize($('textarea'));

//========== IMAGE GALLERY CAROUSEL ==========//
	$('.main-images').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		rows: 0,
		dots: false,
		accessibility: false
	});

	$('.image-gallery-carousel').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		asNavFor: '.main-images',
		rows: 0,
		arrows: true,
		dots: false,
		focusOnSelect: false,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					arrows: false,
					slidesToShow: 6,
				}
			},
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
				}
			},
	 	 ]
	});

	$('.image-gallery-carousel .slick-slide').on('click', function (event) {
    	$('.main-images').slick('slickGoTo', $(this).data('slickIndex'));
	});

//========== TWITTER FEED MOBILE SLIDER ==========//
	$('.tweets').slick({
		responsive: [{
            breakpoint: 9999,
            settings: "unslick"
        },
        {
           	breakpoint: 768,
            settings: {
            	fade: true,
            	cssEase: 'linear',
            	slidesToScroll: 1,
				slidesToShow: 1,
				cssEase: 'linear',
		  		speed: 1000,
				dots: false,
				arrows: false,
				autoplay: true,
				adaptiveHeight: true,
            }
        }]
	});

/* ===== FLIP BOOK ===== */
if($('body').hasClass('single-flipbooks')) {
	var pdf = $('#PDF_Container').attr('data-pdf');
    var options = {
        height: '600px',
        Page_Flip_Sound_Enable: false,
        Open_Table_of_Contents_on_Start: false,
        Allow_PDF_Download: true,
        Flip_Direction: PDFFlip.Flip_Direction.LTR,
        Main_Background_Color: "#ECF1F8",
    };

	var flipBook = $("#PDF_Container").flipBook(pdf, options);
}
})
